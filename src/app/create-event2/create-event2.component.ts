import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-event2',
  templateUrl: './create-event2.component.html',
  styleUrls: ['./create-event2.component.css']
})
export class CreateEvent2Component implements OnInit {

  formularioEvento!: FormGroup;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    // this.formulario = new FormGroup({
    //   nome: new FormControl(),
    //   email: new FormControl()
    // });

    this.formularioEvento = this.formBuilder.group({
      nome: [null, [Validators.required, Validators.minLength(3)]],
      dataInicio: [],
      horaInicio: [],
      dataFim: [],
      horaFim: [],
      descricao: []
    });

  }
  onSubmit(){

    this.http.post(`${environment.baseUrl}/events`, this.formularioEvento.value)
    .subscribe(dados => {

      this.formularioEvento.reset();
      this.router.navigate(['/events']);
       });
    
  }

}
