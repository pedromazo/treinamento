import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  logado: any = false;




  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.logado = this.authService.loggedIn$;

  }

  onLogout(){
    this.authService.logout();
    this.router.navigate(['/']);
  }

}
