import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { Observable } from "rxjs";
import { environment } from "src/environments/environment";


export interface Event {
    nome: string;
    dataInicio: string;
    horaInicio: string;
    dataFim: string;
    horaFim: string;
    descricao: string;
  }

@Injectable()
export class EventsService {

    constructor(private http: HttpClient){}

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      }

    getEvents(): Observable<Event[]>{
        return this.http.get<Event[]>(`${environment.baseUrl}/events`);
    }


}