import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css']
})
export class EditEventComponent implements OnInit {

  parametroId!: number;
  formularioEvento!:FormGroup;

  constructor(private router: Router, private http: HttpClient, private formBuilder: FormBuilder, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.parametroId = this.route.snapshot.params.id;

    this.formularioEvento = this.formBuilder.group({
      nome: [null, [Validators.required, Validators.minLength(3)]],
      dataInit: [],
      horaInit: [],
      dataFim: [],
      horaFim: [],
      descricao: []
    });
    
  }

  onSubmit(){
    this.http.patch(`${environment.baseUrl}/events/${this.parametroId}`, this.formularioEvento.value)
    .subscribe(dados => {
      this.router.navigate(['/events'])
    });
  }

}
