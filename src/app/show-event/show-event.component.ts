import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { ShowEventService } from './show-event.service';
import { EventsService } from '../my-events/my-events.service';


@Component({
  selector: 'app-show-event',
  templateUrl: './show-event.component.html',
  styleUrls: ['./show-event.component.css']
})
export class ShowEventComponent implements OnInit {
  event: any = [];
  eventSub!: Subscription;
  parametroId!: any;
  // dateInit!: Date;
  eService!: EventsService;

  constructor(private showEventService: ShowEventService, 
    private route: ActivatedRoute, 
    private router: Router) {}

  onDelete(form:any) {
    this.showEventService.deleteEvent(this.parametroId).subscribe( dados => {
      this.router.navigate(['/events']);
    });
  }


  ngOnInit(): void {
    this.parametroId = this.route.snapshot.params.id;

    this.eventSub = this.showEventService.getEvent(this.parametroId).subscribe((obj) => {
      this.event = obj;
      // this.dateInit = new Date(this.event.dataInicio);
    });
  }

  ngOnDestroy() {
    this.eventSub.unsubscribe();
  }
}
