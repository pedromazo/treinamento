import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { EventsService } from './my-events.service';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';



@Component({
  selector: 'app-my-events',
  templateUrl: './my-events.component.html',
  styleUrls: ['./my-events.component.css']
})
export class MyEventsComponent implements OnInit, OnDestroy {

  events: any = [];
  events$!: Observable<any>;

  constructor(private eventService: EventsService, private http: HttpClient, private authService: AuthService) {}

  ngOnInit(): void {

    this.events$ = this.eventService.getEvents();
  }

  ngOnDestroy() {

  }

}
