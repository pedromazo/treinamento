import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { ActivatedRoute } from '@angular/router';

import { Observable, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface Event {
  nome: string;
  dataInicio: string;
  horaInicio: string;
  dataFim: string;
  horaFim: string;
  descricao: string;
}

@Injectable({
  providedIn: 'root'
})
export class ShowEventService {

  event: any = [];
  num = 4;
  routeSub!: Subscription;
  parametroId!: any;

  constructor(private http: HttpClient, private route: ActivatedRoute) {}

  getEvent(eventId:number): Observable<Event[]>{
    return this.http.get<Event[]>(`${environment.baseUrl}/events/${eventId}`);
  }

  deleteEvent(eventId:number){
    return this.http.delete(`${environment.baseUrl}/events/${eventId}`);
  }

  
}

