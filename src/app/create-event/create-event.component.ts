import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { RouterLink, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {
  evento: any = {
    nome: '',
    dataInicio: '',
    dataFim: '',
    horaInicio: '',
    horaFim: '',
    descricao: ''
  }
  onSubmit(form: any){
    this.http.post('http://localhost:3000/api/events', this.evento)
    .subscribe(dados => console.log(dados));
    this.router.navigate(['/events']);
  }

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
  }

}
