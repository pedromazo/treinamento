import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService{

  private loggedIn = new BehaviorSubject<boolean>(false);
  loggedIn$ = this.loggedIn.asObservable();

  constructor(private http: HttpClient) { }

  intercept(req: HttpRequest<any>,
    next: HttpHandler): Observable<HttpEvent<any>> {

      const idToken = localStorage.getItem("id_token");

      if (idToken) {
        console.log(idToken)
        const cloned = req.clone({
            headers: req.headers.set("Authorization",
                "Bearer " + idToken)
        });

        return next.handle(cloned);
      }
      else {
        console.log('NOT FOUND')
        return next.handle(req);
      }
  }

  login(formularioUser: any){
    return this.http.post(`${environment.baseUrl}/login`, formularioUser.value).pipe(
      tap((res) => {
        if(true){
        this.loggedIn.next(true);
        }
      })
    );
  };


  setSession(authResult: any){
    const expiresAt = moment().add(authResult.expiresIn,'seconds');
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()));
  }

  logout() {
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
    this.loggedIn.next(false);
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem("expires_at");
    const expiresAt = JSON.parse(expiration || '{}');
    return moment(expiresAt);
  }   
}
