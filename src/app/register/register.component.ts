import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  formularioUser!:FormGroup;

  constructor(private router: Router, private http: HttpClient, private formBuilder: FormBuilder) { }

  ngOnInit(): void {

    this.formularioUser = this.formBuilder.group({
      user: [null, [Validators.required, Validators.minLength(3)]],
      email: [null, Validators.required],
      senha: [null, Validators.required]
    });
  }

  onRegister(){
    try{
      this.http.post(`${environment.baseUrl}/register`, this.formularioUser.value)
      .subscribe(dados => {
        this.router.navigate(['/events']);
      });
    } catch(e){
      console.log(e);
    }
  };

}
