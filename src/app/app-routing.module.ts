import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { CreateEventComponent } from './create-event/create-event.component';
import { CreateEvent2Component } from './create-event2/create-event2.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MyEventsComponent } from './my-events/my-events.component';
import { RegisterComponent } from './register/register.component';
import { ShowEventComponent } from './show-event/show-event.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path:'events', component: MyEventsComponent},
  {path:'events/:id', component: ShowEventComponent},
  {path:'new', component: CreateEvent2Component},
  {path:'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'events/:id/edit', component: EditEventComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
