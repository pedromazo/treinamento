import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';
import * as moment from 'moment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formularioUser!: FormGroup

  constructor(private router: Router, private formBuilder: FormBuilder, private authService: AuthService) { }

  ngOnInit(): void {

    this.formularioUser = this.formBuilder.group({
      user: [null],
      email: [null],
      senha: [null]
    });
  }

  onLogin(){
    try{
        this.authService.login(this.formularioUser).subscribe(dados => {
            this.authService.setSession(dados);
            this.router.navigate(['/events']);
        });
    } catch(e){
      console.log(e);
    }
  }




}
